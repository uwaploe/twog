// Package twog implements the communications protocol used by 2G Engineering's
// linear and rotary actuators.
package twog

import (
	"encoding/hex"
	"fmt"
	"io"
)

type Encoding int

const (
	Standard Encoding = iota
	Addressed
	AsciiStandard
	AsciiAddressed
)

type Delimiter byte

const (
	Invalid             Delimiter = 0
	StartStandard       Delimiter = 0x3c // "<"
	EndStandard                   = 0x3e // ">"
	StartAddressed                = 0x5c // "["
	EndAddressed                  = 0x5d // "]"
	StartAsciiStandard            = 0x28 // "("
	EndAsciiStandard              = 0x29 // ")"
	StartAsciiAddressed           = 0x7b // "{"
	EndAsciiAddressed             = 0x7d // "}"
)

var delimTable map[Delimiter]Delimiter = map[Delimiter]Delimiter{
	StartStandard:       EndStandard,
	StartAddressed:      EndAddressed,
	StartAsciiStandard:  EndAsciiStandard,
	StartAsciiAddressed: EndAsciiAddressed,
}

var encodingTable map[Encoding]Delimiter = map[Encoding]Delimiter{
	Standard:       StartStandard,
	Addressed:      StartAddressed,
	AsciiStandard:  StartAsciiStandard,
	AsciiAddressed: StartAsciiAddressed,
}

// TraceFunc is used to log the payload of incoming and outgoing packets
var TraceFunc func(string, ...interface{})

type CrcError struct {
	expected, got byte
}

func (e *CrcError) Error() string {
	return fmt.Sprintf("Bad crc; expected %02x, got %02x", e.expected, e.got)
}

type FramingError struct{}

func (e *FramingError) Error() string {
	return "packet framing error"
}

type SizeError struct {
	n int
}

func (e *SizeError) Error() string {
	return fmt.Sprintf("Bad packet size; %d bytes", e.n)
}

// ReadPacket returns the payload of the next packet from an io.Reader
func ReadRawPacket(rdr io.Reader) ([]byte, error) {
	var (
		char                 = []byte{0}
		start, end           Delimiter
		r                    io.Reader
		plen, crc, crc_check byte
	)

	// Scan for a valid start delimiter
	for {
		_, err := rdr.Read(char)
		if err != nil {
			return nil, err
		}
		end = delimTable[Delimiter(char[0])]
		if end != Invalid {
			start = Delimiter(char[0])
			break
		}
	}

	// ASCII packets are hex encoded
	switch start {
	case StartAsciiStandard, StartAsciiAddressed:
		r = NewHexDecoder(rdr)
	default:
		r = rdr
	}

	// Read the address byte if necessary
	switch start {
	case StartAddressed, StartAsciiAddressed:
		_, err := r.Read(char)
		if err != nil {
			return nil, err
		}
		crc_check = Crc8(char, crc_check)
	}

	// Read the packet length byte
	_, err := r.Read(char)
	if err != nil {
		return nil, err
	}
	crc_check = Crc8(char, crc_check)
	plen = char[0]

	payload := make([]byte, plen)
	_, err = io.ReadFull(r, payload)
	if err != nil {
		return nil, err
	}

	if TraceFunc != nil {
		TraceFunc("IN: %q\n", payload)
	}

	// Read CRC
	_, err = r.Read(char)
	if err != nil {
		return nil, err
	}
	crc = char[0]

	// Read terminating delimiter
	_, err = rdr.Read(char)
	if err != nil {
		return nil, err
	}

	if Delimiter(char[0]) != end {
		return nil, &FramingError{}
	}

	// Check CRC
	crc_check = Crc8(payload, crc_check)
	if crc != crc_check {
		return nil, &CrcError{got: crc_check, expected: crc}
	}

	return payload, nil
}

type packetEncoder struct {
	w    io.Writer
	enc  Encoding
	addr byte
}

// NewEncoder returns a new 2G packet encoder. All writes result in a single
// packet being created.
func NewEncoder(enc Encoding, addr byte, w io.Writer) io.Writer {
	return &packetEncoder{enc: enc, w: w, addr: addr}
}

// Write writes the contents of p as a single packet. The maximum size of p
// is 255 bytes.
func (e *packetEncoder) Write(p []byte) (int, error) {
	if len(p) > 255 {
		return 0, &SizeError{n: len(p)}
	}

	char := []byte{0}
	start := encodingTable[e.enc]
	end := delimTable[start]

	// Write the start delimiter
	char[0] = byte(start)
	e.w.Write(char)

	var (
		w         io.Writer
		crc_check byte
	)

	// ASCII packets are hex encoded
	switch start {
	case StartAsciiStandard, StartAsciiAddressed:
		w = hex.NewEncoder(e.w)
	default:
		w = e.w
	}

	// Write the address byte if necessary
	switch start {
	case StartAddressed, StartAsciiAddressed:
		char[0] = e.addr
		crc_check = Crc8(char, crc_check)
		w.Write(char)
	}

	// Write the payload length ...
	char[0] = byte(len(p))
	crc_check = Crc8(char, crc_check)
	w.Write(char)
	// ... and the payload
	n, err := w.Write(p)
	if err != nil {
		return n, err
	}

	if TraceFunc != nil {
		TraceFunc("OUT: %q\n", p)
	}

	// Write the CRC and terminating delimiter
	char[0] = Crc8(p, crc_check)
	_, err = w.Write(char)
	if err != nil {
		return n, err
	}

	char[0] = byte(end)
	_, err = e.w.Write(char)

	return n, err
}
