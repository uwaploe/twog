package twog

import (
	"bytes"
	"encoding/binary"
	"io"
)

// Byte order for all binary messages
var ByteOrder binary.ByteOrder = binary.BigEndian

type InvalidResponse struct{}

func (e *InvalidResponse) Error() string {
	return "Invalid response"
}

type PacketType byte

const (
	Goto                PacketType = 0x53
	ReqGoto                        = 0x73
	CurrentLimits                  = 0x49
	ReqCurrentLimits               = 0x69
	FailsafeConfig                 = 0x92
	ReqFailsafeConfig              = 0x93
	FaultConfig                    = 0x4f
	ReqFaultConfig                 = 0x6f
	HardwareBrake                  = 0x86
	ReqHardwareBrake               = 0x87
	MotorControl                   = 0x58
	ReqMotorControl                = 0x78
	OvProtection                   = 0x82
	ReqOvProtection                = 0x83
	PosSampling                    = 0x45
	ReqPosSampling                 = 0x65
	GotoFixedVel                   = 0x55
	ReqGotoFixedVel                = 0x75
	GotoFixedVelExt                = 0x4b
	ReqGotoFixedVelExt             = 0x6b
	Move                           = 0x52
	ReqMove                        = 0x72
	SetZero                        = 0x5a
	ReqSetZero                     = 0x7a
	SysConfig                      = 0x4d
	ReqSysConfig                   = 0x6d
	SysConfig2                     = 0x44
	ReqSysConfig2                  = 0x64
	SysConfig3                     = 0xa6
	ReqSysConfig3                  = 0xa7
	VelocitySet                    = 0x57
	ReqVelocitySet                 = 0x77
	MotionProfile                  = 0x9a
	ReqMotionProfile               = 0x9b
	LoadDumpConfig                 = 0xa4
	ReqLoadDumpConfig              = 0xa5
	StallDetection                 = 0xa8
	ReqStallDetection              = 0xa9
	GainScheduling                 = 0xab
	ReqGainScheduling              = 0xac
	ActuatorName                   = 0xbc
	ReqActuatorName                = 0xbd
	Ack                            = 0x41
	ReqAck                         = 0x61
	FailsafeTimer                  = 0x94
	ReqFailsafeTimer               = 0x95
	FaultHistory                   = 0x4e
	ReqFaultHistory                = 0x6e
	FaultInfo                      = 0x46
	ReqFaultInfo                   = 0x66
	FirmwareBuild                  = 0x96
	ReqFirmwareBuild               = 0x97
	FirmwareVersion                = 0x3f
	ReqFirmwareVersion             = 0x3f
	SystemStatus                   = 0x50
	ReqSystemStatus                = 0x70
	SystemStatus2                  = 0xa2
	ReqSystemStatus2               = 0xa3
	Velocity                       = 0x48
	ReqVelocity                    = 0x68
	ProfileStatus                  = 0x9c
	ReqProfileStatus               = 0x9d
	LoadDefaults                   = 0x40
	ClearFaults                    = 0x21
	ResetRotaryCounters            = 0x3c
	ResetSystem                    = 0x3d
	ReverseDirection               = 0x26
	SaveConfig                     = 0x24
)

type MotorState byte

const (
	MotorOff     MotorState = 0
	MotorOn                 = 1
	MotorOnBrake            = 2
	MotorOnCoast            = 3
	MotorMask               = 7
	BrakeOn                 = (1 << 6)
	BrakePresent            = (1 << 7)
)

type MotorDir byte

const (
	Reverse MotorDir = 0
	Foward           = 1
)

type FailsafeVal struct {
	Enable   uint8  `json:"enable" toml:"enable"`
	Timeout  uint32 `json:"timeout" toml:"timeout"`
	Position int32  `json:"position" toml:"position"`
}

type OvProtectionVal struct {
	Enable uint8  `json:"enable" toml:"enable"`
	Vmax   uint32 `json:"vmax" toml:"vmax"`
}

type StopBehavior uint8

const (
	SeekHold     StopBehavior = 0
	SeekStop                  = 1
	SeekOff                   = 2
	SeekBrake                 = 3
	DriveStop                 = 4
	DriveOff                  = 5
	DriveBrake                = 6
	SeekHwBrake               = 7
	DriveHwBrake              = 8
)

// PV contains the parameters for a positioning operation.
type PV struct {
	// Actuator velocity in mils/min.
	Velocity uint32 `json:"velocity" toml:"velocity"`
	// Position setpoint in mils.
	Position int32 `json:"position" toml:"position"`
	// Distance from the setpoint (in mils) at which the actuator will
	// switch from velocity control to setpoint control.
	Threshold uint32 `json:"threshold" toml:"threshold"`
	// Behavior after the setpoint is reached.
	Stop StopBehavior `json:"stop_behavior" toml:"stop_behavior"`
}

type Status struct {
	State       MotorState `json:"state"`
	Dir         MotorDir   `json:"dir"`
	Position    int32      `json:"pos"`
	Temperature [2]int8    `json:"temp"`
	V           int32      `json:"v"`
	Current     int16      `json:"i"`
}

// Faults contains the response to a system fault information request
type Faults struct {
	// Motor fault bitmask
	Motor uint8 `json:"motor"`
	// Position sensor fault bitmask
	Pos uint8 `json:"pos"`
	// Temperature sensor fault bitmask
	Temp uint8 `json:"temp"`
	// Communication fault bitmask
	Comm uint8 `json:"comm"`
}

// SystemConfig contains the system configuration values
type SystemConfig struct {
	Pgain       float32 `json:"pgain" toml:"pgain"`
	Igain       float32 `json:"igain" toml:"igain"`
	Dgain       float32 `json:"dgain" toml:"dgain"`
	Perr        float32 `json:"perr" toml:"perr"`
	Dclamp      float32 `json:"dclamp" toml:"dclamp"`
	LimitPeriod uint16  `json:"limit_period" toml:"limit_period"`
	MaxMatch    uint16  `json:"max_match" toml:"max_match"`
	PosHigh     int32   `json:"pos_high" toml:"pos_high"`
	PosLow      int32   `json:"pos_low" toml:"pos_low"`
	PowerType   uint8   `json:"power_type" toml:"power_type"`
}

// WritePacket writes a packet type and contents (may be nil) to an io.Writer
func WritePacket(w io.Writer, pt PacketType, contents interface{}) (int, error) {
	var buf bytes.Buffer
	wtr := NewEncoder(Standard, 0, w)
	err := binary.Write(&buf, ByteOrder, pt)
	if err != nil {
		return 0, err
	}

	if contents != nil {
		err = binary.Write(&buf, ByteOrder, contents)
		if err != nil {
			return 0, err
		}
	}
	return wtr.Write(buf.Bytes())
}

// Response represents the reply from the actuator. It provides the packet
// type code and an io.Reader interface with the packet contents.
type Response struct {
	Type PacketType
	data *bytes.Buffer
}

func (r *Response) Read(p []byte) (int, error) {
	return r.data.Read(p)
}

// GetResponse returns the actuator response from an io.Reader
func GetResponse(r io.Reader) (*Response, error) {
	pkt, err := ReadRawPacket(r)
	if err != nil {
		return nil, err
	}
	if len(pkt) == 0 {
		return nil, &InvalidResponse{}
	}
	resp := Response{Type: PacketType(pkt[0]), data: bytes.NewBuffer(pkt[1:])}
	return &resp, nil
}
