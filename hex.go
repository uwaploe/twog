package twog

import (
	"encoding/hex"
	"io"
)

type hexDecoder struct {
	r   io.Reader
	buf []byte
}

func NewHexDecoder(r io.Reader) io.Reader {
	return &hexDecoder{r: r, buf: make([]byte, 255*2)}
}

func (d *hexDecoder) Read(p []byte) (int, error) {
	n := len(p)
	if n > 255 {
		return 0, &SizeError{n: n}
	}

	_, err := d.r.Read(d.buf[0 : n*2])
	if err != nil {
		return 0, err
	}

	return hex.Decode(p, d.buf[0:n*2])
}
