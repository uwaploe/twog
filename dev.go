package twog

import (
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"sync"
	"time"
)

// Read timeout to use for a network connection to the Device
var ResponseTimeout time.Duration = 3 * time.Second

type Device struct {
	port io.ReadWriter
	mu   *sync.Mutex
	name string
}

// Limits contains the current limit parameters for a Device
type Limits struct {
	Actuator float32 `json:"actuator" toml:"actuator"`
	Fallback float32 `json:"fallback" toml:"fallback"`
	Motor    float32 `json:"motor" toml:"motor"`
}

// FwInfo contains the firmware build information
type FwInfo struct {
	Version   string    `json:"version"`
	BuildDate time.Time `json:"build_date"`
	BuildNum  uint      `json:"build_num"`
	Sn        string    `json:"sn"`
}

// NewDevice returns a Device attached to an io.ReadWriter
func NewDevice(port io.ReadWriter, name string) *Device {
	return &Device{
		port: port,
		mu:   &sync.Mutex{},
		name: name}
}

func (d *Device) Name() string {
	return d.name
}

// Request sends a request packet to a Device and returns the response. Body contains
// the request data if required.
func (d *Device) Request(pt PacketType, body interface{}) (*Response, error) {
	d.mu.Lock()
	defer d.mu.Unlock()

	_, err := WritePacket(d.port, pt, body)
	if err != nil {
		return nil, err
	}

	if conn, ok := d.port.(*net.TCPConn); ok {
		conn.SetReadDeadline(time.Now().Add(ResponseTimeout))
		defer conn.SetReadDeadline(time.Time{})
	}
	return GetResponse(d.port)
}

// Status returns the Device status information.
func (d *Device) Status() (Status, error) {
	var stat Status
	resp, err := d.Request(ReqSystemStatus, nil)
	if err != nil {
		return stat, err
	}

	err = binary.Read(resp, ByteOrder, &stat)
	return stat, err
}

func (d *Device) Faults() (Faults, error) {
	var f Faults
	resp, err := d.Request(ReqFaultInfo, nil)
	if err != nil {
		return f, err
	}

	err = binary.Read(resp, ByteOrder, &f)
	return f, err
}

// Configuration returns the current system configuraton values.
func (d *Device) Configuration() (SystemConfig, error) {
	var cfg SystemConfig
	resp, err := d.Request(ReqSysConfig, nil)
	if err != nil {
		return cfg, err
	}

	err = binary.Read(resp, ByteOrder, &cfg)
	return cfg, err
}

func (d *Device) SetLimits(l Limits) error {
	x := struct {
		Board     uint32
		Reduction uint8
		Motor     uint16
	}{}
	x.Board = uint32(l.Actuator * 1000)
	x.Reduction = uint8(l.Fallback)
	x.Motor = uint16(l.Motor * 1000)

	_, err := d.Request(CurrentLimits, x)
	return err
}

func (d *Device) Limits() (Limits, error) {
	var l Limits
	x := struct {
		Board     uint32
		Reduction uint8
		Motor     uint16
	}{}

	resp, err := d.Request(ReqCurrentLimits, nil)
	if err != nil {
		return l, err
	}
	err = binary.Read(resp, ByteOrder, &x)
	if err != nil {
		return l, err
	}

	l.Actuator = float32(x.Board) / 1000.
	l.Fallback = float32(x.Reduction)
	l.Motor = float32(x.Motor) / 1000.

	return l, nil
}

// SetState sets the motor state to the value of "s".
func (d *Device) SetState(s MotorState) error {
	_, err := d.Request(MotorControl, s)
	return err
}

// State returns the current motor state
func (d *Device) State() (MotorState, error) {
	var s MotorState
	resp, err := d.Request(ReqMotorControl, nil)
	if err != nil {
		return s, err
	}
	err = binary.Read(resp, ByteOrder, &s)
	return s, err
}

// Save stores the current actuator configuration in EEPROM.
func (d *Device) Save() error {
	_, err := d.Request(SaveConfig, nil)
	return err
}

// SetZeroPoint sets the zero point for future relative movements.
func (d *Device) SetZeroPoint(x uint32) error {
	_, err := d.Request(SetZero, x)
	return err
}

// ZeroPoint returns the current zero point.
func (d *Device) ZeroPoint() (uint32, error) {
	var x uint32
	resp, err := d.Request(ReqSetZero, nil)
	if err != nil {
		return x, err
	}
	err = binary.Read(resp, ByteOrder, &x)
	return x, err
}

// Goto starts an actuator movement to an absolute setpoint.
func (d *Device) Goto(p PV) error {
	_, err := d.Request(GotoFixedVelExt, p)
	return err
}

// PollStatus reads the device status at the specified interval until the context
// is cancelled and sends the values to the supplied channel. If the interval is
// zero, the status will be read as fast as possible.
func (d *Device) PollStatus(ctx context.Context, interval time.Duration,
	ch chan<- Status) error {

	if interval == 0 {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
				stat, err := d.Status()
				if err != nil {
					return err
				}
				select {
				case ch <- stat:
				default:
				}
			}
		}
	} else {
		t := time.NewTicker(interval)
		defer t.Stop()
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-t.C:
				stat, err := d.Status()
				if err != nil {
					return err
				}
				select {
				case ch <- stat:
				default:
				}
			}
		}
	}

	return nil
}

// WaitForSetpoint returns when an absolute setpoint in reached or the
// supplied Context is cancelled. If ch is non-nil, the device status
// will be sent to it everytime the device is polled.
func (d *Device) WaitForSetpoint(ctx context.Context, pos int32,
	ch chan<- Status) error {
	errch := make(chan error, 1)
	stch := make(chan Status, 1)

	childCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	go func() { errch <- d.PollStatus(childCtx, 0, stch) }()

loop:
	for {
		select {
		case err := <-errch:
			return err
		case stat := <-stch:
			select {
			case ch <- stat:
			default:
			}
			if stat.Position == pos {
				break loop
			}
		}
	}

	return nil
}

// SysInfo returns the hardware and software information for the device.
func (d *Device) SysInfo() (FwInfo, error) {
	fbi := struct {
		BuildNum  uint32
		BuildTime uint64
		Sn        [4]uint32
		_         [2]uint32
	}{}
	var fi FwInfo

	resp, err := d.Request(ReqFirmwareBuild, nil)
	if err != nil {
		return fi, fmt.Errorf("ReqFirmwareBuild: %w", err)
	}

	err = binary.Read(resp, ByteOrder, &fbi)
	if err != nil {
		return fi, fmt.Errorf("FirmwareBuild decode: %w", err)
	}

	fv := struct {
		Major uint16
		Minor uint16
	}{}
	resp, err = d.Request(ReqFirmwareVersion, nil)
	if err != nil {
		return fi, fmt.Errorf("ReqFirmwareVersion: %w", err)
	}

	err = binary.Read(resp, ByteOrder, &fv)
	if err != nil {
		return fi, fmt.Errorf("FirmwareVersion decode: %w", err)
	}

	fi.Version = fmt.Sprintf("%d.%d", fv.Major, fv.Minor)
	fi.BuildNum = uint(fbi.BuildNum)
	fi.BuildDate = time.Unix(int64(fbi.BuildTime), 0)
	fi.Sn = fmt.Sprintf("%08x-%08x-%08x-%08x", fbi.Sn[0], fbi.Sn[1],
		fbi.Sn[2], fbi.Sn[3])

	return fi, nil
}
