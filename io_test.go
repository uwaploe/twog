package twog

import (
	"bytes"
	"testing"
)

func TestReadWrite(t *testing.T) {
	var buf bytes.Buffer

	w := NewEncoder(Standard, 0, &buf)
	payload := []byte("hello world")
	n, err := w.Write(payload)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("buf: %q", buf.Bytes())

	if n != len(payload) {
		t.Errorf("Bad data length; expected %d, got %d", len(payload), n)
	}

	pkt, err := ReadRawPacket(&buf)
	if err != nil {
		t.Fatal(err)
	}

	if string(pkt) != string(payload) {
		t.Errorf("Data error; expected %q, got %q", payload, pkt)
	}
}

func TestAsciiReadWrite(t *testing.T) {
	var buf bytes.Buffer

	w := NewEncoder(AsciiStandard, 0, &buf)
	payload := []byte("hello world")
	n, err := w.Write(payload)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("buf: %q", buf.Bytes())

	if n != len(payload) {
		t.Errorf("Bad data length; expected %d, got %d", len(payload), n)
	}

	pkt, err := ReadRawPacket(&buf)
	if err != nil {
		t.Fatal(err)
	}

	if string(pkt) != string(payload) {
		t.Errorf("Data error; expected %q, got %q", payload, pkt)
	}
}
